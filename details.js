$(() =>{
    const urlParams = new URLSearchParams(window.location.search);
    const pokemonId = urlParams.get('pokemonId');
    
   


    axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemonId}`)
    .then(response =>{


        $("h3").html(response.data.name);
        //alert(response.data.sprites.front_default);
        $("#mainImage").attr("src", response.data.sprites.front_default);
    })
})


function showModal(){
    $('.ui.modal').modal('show');
    
}