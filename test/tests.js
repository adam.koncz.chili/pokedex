var test = require('unit.js');
var logic = require('../logic.js');

describe('Tesztesetek különböző hosszúságú id-kra', function(){
  it('3 számjegyű id-ra', function(){
    let number = logic.getIdFromUrl("https://pokeapi.co/api/v2/pokemon/201/");
    test.number(number).isEqualTo(201);
  });
  it('2 számjegyű id-ra', function(){
    let number = logic.getIdFromUrl("https://pokeapi.co/api/v2/pokemon/20/");
    test.number(number).isEqualTo(20);
  });
  it('1 számjegyű id-ra', function(){
    let number = logic.getIdFromUrl("https://pokeapi.co/api/v2/pokemon/2/");
    test.number(number).isEqualTo(2);
  });
});

