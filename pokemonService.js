const PokemonService = {

    numberOfPokemons: pokemonsdata.length,

    actualPokemons:null,

    getPokemons: function (limit, offset){
        if(this.actualPokemons == null){
            this.actualPokemons = pokemonsdata;
            this.numberOfPokemons = pokemonsdata.length;
        }
        return this.actualPokemons.slice(+offset, +offset + +limit);
    },

    getAllTypes: function(){
        let set = new Set();
        pokemonsdata.map(pokemon => pokemon.types).flat(2).forEach(set.add, set);
        return set;
    },

    getMinAndMaxWeight: function(){
        let arrayOfWeights = pokemonsdata.map(pokemon => pokemon.weight);
        let min = Math.min(...arrayOfWeights);
        let max = Math.max(...arrayOfWeights);
        let res = {};
        res.minWeight = min;
        res.maxWeight = max;
        return res;

    },

    filterPokemons: function(filterData){
        let resultArray = pokemonsdata;
        
        if(filterData.name){
            resultArray = resultArray.filter(pokemon => pokemon.name.includes(filterData.name));
        }
        
        if(filterData.types){
            if(!Array.isArray(filterData.types)){
                filterData.types = [].concat(filterData.types)
            }

            resultArray = resultArray.filter(pokemon =>{
                let res = true;
                filterData.types.forEach(type =>{
                    res &= pokemon.types.includes(type)
                })
                return res;
            })
        }

        if(filterData.isDefault){
            resultArray = resultArray.filter(pokemon =>{
                return pokemon.isDefault
            })
        }

        if(filterData.minimumWeight){
            resultArray = resultArray.filter(pokemon =>{
                return pokemon.weight >= filterData.minimumWeight;
            })
        }

        if(filterData.maximumWeight){
            resultArray = resultArray.filter(pokemon =>{
                return pokemon.weight <= filterData.maximumWeight;
            })
        }

        this.numberOfPokemons = resultArray.length;
        this.actualPokemons = resultArray;
    }

};



