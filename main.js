  let offset = 0;  
  const limit = 21;  
  let pokemons = PokemonService.getPokemons(limit, offset);
  let numberOfPokemons = PokemonService.numberOfPokemons;

$(()=>{
  init();
})

function init(){
  $('#PokémonNumberDetail').html(numberOfPokemons)
  $('#sidebar').sidebar('attach events', '.toc.item')
  $('#filterAccordion').accordion()
  setUpForm();
  setUpDropdown();
  setUpPaginationMenu();
  renderPokemons(pokemons);
}

function setUpForm(){
  let minAndMaxWeight = PokemonService.getMinAndMaxWeight();
  $("#minimumWeightLabel").html(`Minimum Weight (Min ${minAndMaxWeight.minWeight})`)
  $("#maximumWeightLabel").html(`Maximum Weight (Max ${minAndMaxWeight.maxWeight})`)
  
  $.fn.form.settings.rules.smallerThanMax = function(param) {
    let max = $("#maximumWeightInput").val();
    if(!max)
      return true;
    return +param <= +max;
  }

  $.fn.form.settings.rules.greaterThanMin = function(param) {
    let min = $("#minimumWeightInput").val();
    if(!min)
      return true;
    return +param >= +min;
  }

  $('#filterForm').form({
    fields: {
      minimumWeight: {
        identifier: 'minimumWeight',
        optional: true,
        rules: [
          {
            type : `integer[${minAndMaxWeight.minWeight}..${minAndMaxWeight.maxWeight}]`,
            prompt: 'Please enter some valid weight'
          },
          {
            type   : `smallerThanMax`,
            prompt : 'Minimum should be smaller than maximum'
          }
        ]
      },
      maximumWeight: {
        identifier: 'maximumWeight',
        optional: true,
        rules: [
          {
            type : `integer[${minAndMaxWeight.minWeight}..${minAndMaxWeight.maxWeight}]`,
            prompt: 'Please enter some valid weight'
          },
          {
            type   : `greaterThanMin`,
            prompt : 'Maximum should be greater than minimum'
          }
        ]
      }
    }
  })

  $('#filterForm').submit(function(e) {
    e.preventDefault();
    if( $('#filterForm').form('is valid')) {
      
      let data = $('#filterForm').serializeArray().reduce(function(obj, item) {
      if(item.value.indexOf(',') > 0){
        item.value = item.value.split(',')
      }
      obj[item.name] = item.value;
      return obj;
      }, {});
      PokemonService.filterPokemons(data);
      offset = 0;
      pokemons = PokemonService.getPokemons(limit, offset);
      numberOfPokemons = PokemonService.numberOfPokemons;
      $('#PokémonNumberDetail').html(numberOfPokemons)
      if(numberOfPokemons == 0){
        $('#noResultsMessage').removeClass('hidden')
        $('#paginationMenu').css('display', 'none')
      } else {
        $('#noResultsMessage').addClass('hidden')
        $('#paginationMenu').css('display', 'inline-flex')
      }


      setUpPaginationMenu();
      renderPokemons(pokemons);
    
    }

});
}

function setUpDropdown(){
  let dropdownValues = [];
  let types = PokemonService.getAllTypes();

  types.forEach(type =>{
    let value = {};
    value.value = type;
    value.name = `<image class="ui mini image" src="./resources/${type}.png" />${type}`
    dropdownValues.push(value);
  })

  dropdownValues.sort((a,b) =>{
    const typeA = a.value;
    const typeB = b.value;
    if (typeA > typeB){
      return 1;
    } else if (typeA < typeB){
      return -1;
    }
    return 0;
  });
  
  $('#filter-type')
  .dropdown({
    values: dropdownValues,  
    maxSelections: 2,
    placeholder: 'Types',
  });
}

function setUpPaginationMenu(){
  if(numberOfPokemons < limit){
    $('#paginationMenu').css('display', 'none');
  } else {
    $('#paginationMenu').css('display', 'inline-flex')
  }

  
  configurePreviousPageArrow();
  configureNextPageArrow();
  
  const numOfPages = Math.ceil(numberOfPokemons / limit);
  const actualPage = 1 + (offset / limit)
  $("#middlePage").remove();
  $("#firstPage").off('click')
  $("#lastPage").off('click')
  $("#lastPage").removeClass("active");

  if(actualPage == 1){
    $("#firstPage").addClass("active");
    $("#firstPage").css("cursor", "default");
  } else {
    $("#firstPage").removeClass("active");
    $("#firstPage").click(() => modifyOffsetWithNumberAndReloadPokemons(-(+offset)));
  }
  
  $('#lastPage').html(numOfPages)
  
  if(actualPage != numOfPages && actualPage != 1){
    $('#firstPage').after(`<a id="middlePage" class="active item" style="cursor:default">
    ${actualPage}
                           </a>`);
  } 
  
  $("#lastPage").click(() => modifyOffsetWithNumberAndReloadPokemons(-(+offset) + (numOfPages-1) * limit ));
  if(actualPage == numOfPages) {
    $("#lastPage").addClass("active");
    $("#lastPage").css("cursor", "default");
  }
}

function configurePreviousPageArrow(){
  $('#previousPageArrow').off('click');
  if(offset == 0){
    $('.left.chevron').addClass("disabled");
  } else {
    $('.left.chevron').removeClass("disabled");
    $('#previousPageArrow').click(() => modifyOffsetWithNumberAndReloadPokemons(-(+limit)));
  } 
}

function configureNextPageArrow(){
  $('#nextPageArrow').off('click');
  if(+offset + +limit >= numberOfPokemons){
    $('.right.chevron').addClass("disabled");
  } else {
    $('.right.chevron').removeClass("disabled");
    $('#nextPageArrow').click(() => modifyOffsetWithNumberAndReloadPokemons(+limit));
  } 
}

function modifyOffsetWithNumberAndReloadPokemons(number){
  offset += +number;
  pokemons = PokemonService.getPokemons(limit, offset);
  renderPokemons(pokemons);
  setUpPaginationMenu();
}

function renderPokemons(pokemons){
  $("#cards").empty();
  pokemons.forEach(pokemon =>{
    addCard(pokemon.sprite, pokemon.name, pokemon.types, pokemon.baseExperience, pokemon.Id );
  })
}

function getHtmlBelowImage(name, baseExp, types){
  let imgs = "";
  types.forEach(type =>{
    imgs += ` <img  class="ui mini image" src="./resources/${type}.png" title=${type}>`
  })
  
  return (
    `<div class="content" style="display:flex; justify-content:space-between">
      <div>
      <div class="header" style="font-size:20px">${name}</div>
      <div class="meta">Base experience: ${baseExp} </div>
      </div>
      <div style="margin-left:auto; margin-right:0;">
          ${imgs}
       
        </div>
    </div>
  </div>`
);
}

function addCard(image, name, types, baseExp, id){
  if(!image){
    image = "./resources/pokeball.png";
    $( "#cards" ).append(`
        <div class="ui card">
        <div class="image" onclick="showDetailsModal(${id})">
          <img src="${image}" style="cursor: pointer">
        </div>
        ${getHtmlBelowImage(name, baseExp, types)}  
    `)
  } else {
    $( "#cards" ).append(`
    <div class="ui card">
    <div id="image${id}Container" class="image" onclick="showDetailsModal(${id})">
      <div id="placeholder${id}" class="ui placeholder">
      <div class="square image"></div>
      </div>
      <img id="image${id}" src="${image}" style="cursor: pointer; display:none">
      </div>
      ${getHtmlBelowImage(name, baseExp, types)}  
  `)
   $(`#image${id}Container`).imagesLoaded(() => {
    $(`#placeholder${id}`).remove();
    $(`#image${id}`).css('display', 'unset');
   })
  }
}
var modalInitialized = false


function showDetailsModal(id){
  $('#modal').modal('show');
  if(!modalInitialized){
    $('.sprite-images').slick({
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 1
    });
    modalInitialized = true;
  }
  axios.get(`https://pokeapi.co/api/v2/pokemon/${id}`)
  .then(response =>{
    $("#moodalHeader").html(response.data.name)
    $("#weightContent").html(response.data.weight)
    $("#baseExpContent").html(response.data.base_experience)
    $("#attackBar").progress({
      percent: response.data.stats[4].base_stat - 1,
      autosucces:false

    })
    $("#speedBar").progress({
      percent: response.data.stats[0].base_stat - 1,
      autosucces:false
    })

    let imageNumber = 0;
    for(let [key,value] of Object.entries(response.data.sprites)){
      if(value){
        imageNumber++;
        $('.sprite-images').slick('slickAdd', `
        <div style="font-size:25px">
        <img class="ui medium image" src="${value}">
        ${key.replace(/_/g, ' ')}
        </div>
        `)
      }
    }



    removeSpriteImagesFromModal($('.slick-track').children().length - imageNumber)

    })
    
    //$('.sprite-images').slick('slickRemove', 0);

  

}

function removeSpriteImagesFromModal(length){

  for(let i = 0; i < length; i++){
    $('.sprite-images').slick('slickRemove', 0);
  }
}